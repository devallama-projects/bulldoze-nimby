﻿using ICities;
using UnityEngine;
using System;

namespace BulldozeNIMBY {
    public class BuildingMonitor : BuildingExtensionBase {
        public override void OnBuildingReleased(ushort buildingID) {
            try {
                if (!BulldozePriceManager.Instance.IsCurrentBuilding(buildingID)) {
                    BulldozePriceManager.Instance.SetBuilding(buildingID);
                }

                if (BulldozePriceManager.Instance.Cost != 0) {
                    BulldozePriceManager.Instance.BuildingDestroyed();
                }
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] BulldozeNIMBY:OnBuildingReleased -> Exception: " + e.Message);
            }
        }
    }
}
