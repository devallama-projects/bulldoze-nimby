﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;
using BulldozeNIMBY.UI;

namespace BulldozeNIMBY {
    class Bulldoze : MonoBehaviour {
        private UILabelBulldozeCost _label;

        private void Awake() {
            try {
                CreateUI();
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:Awake -> Exception: " + e.Message);
            }
        }

        private void OnEnable() {
            try {

            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:OnEnable -> Exception: " + e.Message);
            }
        }

        private void Start() {
            try {
                
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:Start -> Exception: " + e.Message);
            }
        }

        private void Update() {
            try {
                BulldozeTool bulldozeTool = Singleton<BulldozeTool>.instance;

                if (bulldozeTool.enabled) {
                    ushort buildingID = bulldozeTool.GetHoverInstanceBuilding();

                    if (!BulldozePriceManager.Instance.IsCurrentBuilding(buildingID)) {
                        BulldozePriceManager.Instance.SetBuilding(buildingID);

                        _label.SetCost(BulldozePriceManager.Instance.Cost);
                    }
                }
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:Update -> Exception: " + e.Message);
            }
        }

        private void OnDisable() {
            try {

            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:OnDisable -> Exception: " + e.Message);
            }
        }

        private void OnDestroy() {
            try {

            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:OnDestroy -> Exception: " + e.Message);
            }
        }

        private void CreateUI() {
            try {
                UITiledSprite _bulldozerBar = GameObject.Find("BulldozerBar").GetComponent<UITiledSprite>();
                _label = (UILabelBulldozeCost)_bulldozerBar.AddUIComponent(typeof(UILabelBulldozeCost));
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Bulldozer:CreateUI -> Exception: " + e.Message);
            }
        }
    }
}
