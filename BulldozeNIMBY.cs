﻿using ColossalFramework;
using System;
using ICities;
using UnityEngine;
using ColossalFramework.UI;
using System.Collections.Generic;

namespace BulldozeNIMBY {

    public class BulldozeNIMBY : IUserMod {
        public string Name {
            get { return "Bulldoze NIMBY"; }
        }

        public string Description {
            get { return "Bulldozing NIMBYs"; }
        }

        public string Version {
            get { return "0.1.0"; }
        }

        public BulldozeNIMBY() {
            new Configuration(
                Version,
                ConfigurationOptions.ConfigPath,
                ConfigurationOptions.OptionGroups
            );
        }

        public void OnSettingsUI(UIHelperBase helper) {
            // Load the configuration
            Configuration.Instance.Load();
            Configuration.Instance.SettingsUI(helper);
        }
    }
}
