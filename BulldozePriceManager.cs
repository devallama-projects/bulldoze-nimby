﻿using ColossalFramework;
using ICities;
using System;
using UnityEngine;

namespace BulldozeNIMBY {
    public class BulldozePriceManager {
        private static BulldozePriceManager instance = new BulldozePriceManager();

        public static BulldozePriceManager Instance { get { return instance; } }

        private Building building;
        private BuildingInfo buildingInfo;
        private ushort buildingID = 0;

        private readonly BuildingManager buildingManager = Singleton<BuildingManager>.instance;
        private readonly ConfigOptionsGroup baseCosts = Configuration.Instance.OptionGroups["BaseCosts"];
        private readonly ConfigOptionsGroup multipliers = Configuration.Instance.OptionGroups["Multipliers"];

        public int Cost { get; private set; } = 0;

        public void SetBuilding(ushort buildingID) {
            this.buildingID = buildingID;
            building = FindBuilding(buildingID);
            buildingInfo = building.Info;

            GetCost();
        }

        public void BuildingDestroyed() {
            if (buildingInfo != null) {
                Singleton<EconomyManager>.instance.FetchResource(EconomyManager.Resource.FeePayment, Cost, buildingInfo.m_class);
            }
        }

        public bool IsCurrentBuilding(ushort buildingID) {
            return this.buildingID == buildingID;
        }

        private Building FindBuilding(ushort buildingID) {
            return buildingManager.m_buildings.m_buffer[buildingID];
        }

        private void GetCost() {
            if (buildingInfo != null && baseCosts != null) {
                ItemClass.Service buildingService = buildingInfo.m_class.m_service;

                if (
                    buildingService == ItemClass.Service.Residential ||
                    buildingService == ItemClass.Service.Industrial ||
                    buildingService == ItemClass.Service.Commercial ||
                    buildingService == ItemClass.Service.Office
                ) {
                    int? baseCost = null;

                    foreach (int service in (int[])Enum.GetValues(typeof(ItemClass.SubService))) {
                        if ((int)buildingInfo.m_class.m_subService == service) {
                            string name = Enum.GetName(typeof(ItemClass.SubService), service);

                            try {
                                baseCost = baseCosts.Options[name].TryGetValueInt();
                            } catch (Exception e) {
                                Debug.LogWarning(e);
                            }
                        }
                    }

                    if (baseCost != null) {
                        Cost = CalculateCost((int)baseCost);
                    } else {
                        Cost = 0;
                    }
                } else {
                    if (Cost != 0) {
                        Cost = 0;
                    }
                }
            }
        }

        private int CalculateCost(int baseCost) {
            if (multipliers != null) {
                int width = building.m_width;
                int length = building.m_length;
                int population = building.m_citizenCount;
                int level = building.m_level;
                double mArea = (double)multipliers.Options["Area"].TryGetValueDouble();
                double mPop = (double)multipliers.Options["Population"].TryGetValueDouble();
                double mLevel = (double)multipliers.Options["Level"].TryGetValueDouble();

                return Convert.ToInt32(((baseCost * (width * length * mArea)) + (population * mPop)) * (1 + (level * mLevel)) * 100);

                // return Convert.ToInt32(((baseCost * (width * length * mArea) * Math.Max(1, level * mLevel)) + (population * mPop)) * 100);
            } else {
                return 0;
            }
        }
    }
}
