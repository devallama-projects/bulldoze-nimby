﻿using ColossalFramework;
using System;
using ICities;
using UnityEngine;
using ColossalFramework.UI;
using System.Reflection;


namespace BulldozeNIMBY {
    public static class BulldozeToolExtensions {
        public static ushort GetHoverInstanceBuilding(this BulldozeTool bulldozeTool) {
            if (bulldozeTool == null) {
                throw new ArgumentException("Bulldoze tool not found!");
            }

            var fieldInfo = typeof(BulldozeTool).GetField("m_hoverInstance", BindingFlags.Instance | BindingFlags.NonPublic);
            InstanceID hoverInstance = (InstanceID)fieldInfo.GetValue(bulldozeTool);
            ushort building = hoverInstance.Building;

            return building;
        }
    }
}
