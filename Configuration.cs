﻿using ICities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace BulldozeNIMBY {
    public class ConfigOption {
        public string Label { get; private set; }
        public string Value { get; private set; }
        public string OriginalType { get; private set; }

        public ConfigOption(string label, string value) {
            Label = label;
            Value = value;
        }
        public ConfigOption(string label, string value, string originalType) {
            Label = label;
            Value = value;
            OriginalType = originalType;
        }

        public ConfigOption(string label, int value) {
            Label = label;
            Value = value.ToString();
            OriginalType = value.GetType().ToString();
        }

        public ConfigOption(string label, double value) {
            Label = label;
            Value = value.ToString();
            OriginalType = value.GetType().ToString();
        }

        public void SetValue(string value) {
            Value = value;
        }

        public void SetValue(int value) {
            Value = value.ToString();
            OriginalType = value.GetType().ToString();
        }

        public void SetValue(double value) {
            Value = value.ToString();
            OriginalType = value.GetType().ToString();
        }

        public string GetValue() {
            return Value;
        }

        public int? TryGetValueInt() {
            if (int.TryParse(Value, out int result)) {
                return result;
            }
            return null;
        }

        public double? TryGetValueDouble() {
            if (double.TryParse(Value, out double result)) {
                return result;
            }
            return null;
        }
    }

    public class ConfigOptionsGroup {
        public string Name;
        public Dictionary<string, ConfigOption> Options;

        public ConfigOptionsGroup(string name, Dictionary<string, ConfigOption> options) {
            Name = name;
            Options = options;
        }
    }

    [Serializable]
    public class ConfigDataGroup {
        public string Key;
        public string Name;
        public List<ConfigDataItem> Options;

        public ConfigDataGroup(string key, string name, List<ConfigDataItem> options) {
            Key = key;
            Name = name;
            Options = options;
        }

        public ConfigDataGroup() { }
    }

    [Serializable]
    public class ConfigDataItem {
        public string Key;
        public string Label;
        public string Value;
        public string OriginalType;

        public ConfigDataItem(string key, string label, string value, string type) {
            Key = key;
            Label = label;
            Value = value;
            OriginalType = type;
        }

        public ConfigDataItem() { }
    }

    public class Configuration {
        private static Configuration instance = null;
        public static Configuration Instance {
            get {
                return instance;
            }
        }

        public bool ConfigUpdated { get; set; }
        public Dictionary<string, ConfigOptionsGroup> OptionGroups { get; private set; }

        public readonly Dictionary<string, ConfigOptionsGroup> defaultOptionGroups;
        private readonly string configPath = "";
        private readonly string version = "";

        public Configuration(
            string version,
            string configPath,
            Dictionary<string, ConfigOptionsGroup> optionGroups
        ) {
            this.version = version;
            this.configPath = configPath;
            OptionGroups = optionGroups;
            defaultOptionGroups = CloneOptionGroups(optionGroups);

            if (instance == null) {
                instance = this;
            }
        }

        public Dictionary<string, ConfigOptionsGroup> CloneOptionGroups(Dictionary<string, ConfigOptionsGroup> optionGroups) {
            Dictionary<string, ConfigOptionsGroup> clonedOptionGroups = new Dictionary<string, ConfigOptionsGroup>();

            foreach (KeyValuePair<string, ConfigOptionsGroup> group in optionGroups) {
                Dictionary<string, ConfigOption> optionItems = new Dictionary<string, ConfigOption>();

                foreach (KeyValuePair<string, ConfigOption> item in group.Value.Options) {
                    optionItems[item.Key] = new ConfigOption(item.Value.Label, item.Value.Value, item.Value.OriginalType);
                }

                clonedOptionGroups[group.Key] = new ConfigOptionsGroup(group.Value.Name, optionItems);
            }

            return clonedOptionGroups;
        }

        public void Save() {
            List<ConfigDataGroup> tempDataGroups = new List<ConfigDataGroup>(OptionGroups.Count);

            foreach (KeyValuePair<string, ConfigOptionsGroup> optionGroup in OptionGroups) {
                List<ConfigDataItem> tempDataItems = new List<ConfigDataItem>(optionGroup.Value.Options.Count);

                foreach (KeyValuePair<string, ConfigOption> item in optionGroup.Value.Options) {
                    tempDataItems.Add(new ConfigDataItem(item.Key, item.Value.Label, item.Value.Value, item.Value.OriginalType));
                }

                tempDataGroups.Add(new ConfigDataGroup(optionGroup.Key, optionGroup.Value.Name, tempDataItems));
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<ConfigDataGroup>));
            XmlSerializerNamespaces noNamespaces = new XmlSerializerNamespaces();
            noNamespaces.Add("", "");

            try {
                using (var streamWriter = new System.IO.StreamWriter(configPath)) {
                    serializer.Serialize(streamWriter, tempDataGroups, noNamespaces);
                }
            } catch (Exception e) {
                Debug.LogException(e);
            }
        }

        public void Load() {
            Dictionary<string, ConfigOptionsGroup> optionGroups = OptionGroups;
            XmlSerializer serializer = new XmlSerializer(typeof(List<ConfigDataGroup>));

            try {
                if (File.Exists(configPath)) {
                    using (StreamReader streamReader = new System.IO.StreamReader(configPath)) {
                        List<ConfigDataGroup> tempDataGroups = (List<ConfigDataGroup>)serializer.Deserialize(streamReader);

                        foreach (ConfigDataGroup group in tempDataGroups) {
                            Dictionary<string, ConfigOption> optionItems = new Dictionary<string, ConfigOption>();

                            foreach (ConfigDataItem item in group.Options) {
                                optionItems[item.Key] = new ConfigOption(item.Label, item.Value, item.OriginalType);
                            }

                            optionGroups[group.Key] = new ConfigOptionsGroup(group.Name, optionItems);
                        }

                        OptionGroups = optionGroups;
                    }
                }
            } catch (Exception e) {
                Debug.LogException(e);
            }
        }

        public void Reset() {
            OptionGroups = CloneOptionGroups(defaultOptionGroups);
            Save();
        }

        public void SettingsUI(UIHelperBase helper) {
            UIHelperBase resetGroup = helper.AddGroup("Reset settings - Requires restart");

            resetGroup.AddButton("Reset settings", () => {
                Reset();
            });

            foreach (KeyValuePair<string, ConfigOptionsGroup> optionGroup in OptionGroups) {
                UIHelperBase group = helper.AddGroup(optionGroup.Value.Name);

                foreach (KeyValuePair<string, ConfigOption> option in optionGroup.Value.Options) {
                    group.AddTextfield(option.Value.Label, Instance.OptionGroups[optionGroup.Key].Options[option.Key].Value, text => {
                        switch (option.Value.OriginalType) {
                            case "System.Int32":
                                if (int.TryParse(text, out int resultInt)) {
                                    Instance.OptionGroups[optionGroup.Key].Options[option.Key].SetValue(resultInt);
                                    Save();
                                }
                                break;
                            case "System.Double":
                                if (double.TryParse(text, out double resultDouble)) {
                                    Instance.OptionGroups[optionGroup.Key].Options[option.Key].SetValue(resultDouble);
                                    Save();
                                }
                                break;
                            default:
                                Instance.OptionGroups[optionGroup.Key].Options[option.Key].SetValue(text);
                                Save();
                                break;
                        }
                    });
                }
            }
        }
    }
}