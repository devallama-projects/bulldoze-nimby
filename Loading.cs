﻿using ColossalFramework;
using System;
using ICities;
using UnityEngine;
using ColossalFramework.UI;

namespace BulldozeNIMBY {
    public class Loading : LoadingExtensionBase {
        private LoadMode _loadMode;
        private GameObject _gameObject;

        public override void OnLevelLoaded(LoadMode mode) {
            try {
                _loadMode = mode;

                if (_loadMode != LoadMode.LoadGame && _loadMode != LoadMode.NewGame && _loadMode != LoadMode.NewGameFromScenario) {
                    return;
                }

                UIView objectOfType = UnityEngine.Object.FindObjectOfType<UIView>();
                if (objectOfType != null) {
                    _gameObject = new GameObject("BulldozeNIMBY");
                    _gameObject.transform.parent = objectOfType.transform;
                    _gameObject.AddComponent<Bulldoze>();
                }

               Configuration.Instance.Load();
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Loading:OnLevelLoaded -> Exception: " + e.Message);
            }
        }

        public override void OnLevelUnloading() {
            try {
                if (_loadMode != LoadMode.LoadGame && _loadMode != LoadMode.NewGame && _loadMode != LoadMode.NewGameFromScenario) {
                    return;
                }

                if (_gameObject == null) {
                    return;
                }

                UnityEngine.Object.Destroy(_gameObject);
            } catch (Exception e) {
                Debug.Log("[BulldozeNIMBY] Loading:OnLevelUnloading -> Exception: " + e.Message);
            }
        }
    }
}
