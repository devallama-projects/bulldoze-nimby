﻿using System.Collections.Generic;

namespace BulldozeNIMBY {
    static public class ConfigurationOptions {
        static public string ConfigPath { get; private set; } = "BulldozeNIMBY.xml";
        static public Dictionary<string, ConfigOptionsGroup> OptionGroups { get; } = new Dictionary<string, ConfigOptionsGroup>() {
            {
                "BaseCosts",
                new ConfigOptionsGroup(
                    "Base costs",
                    new Dictionary<string, ConfigOption>() {
                        // Residential
                        { "ResidentialLow", new ConfigOption("Residential - Low", 15) },
                        { "ResidentialLowEco", new ConfigOption("Residential - LowEco", 15) },
                        { "ResidentialHigh", new ConfigOption("Residential - High", 25) },
                        { "ResidentialHighEco", new ConfigOption("Residential - HighEco", 25) },
                        // Commercial
                        { "CommercialEco", new ConfigOption("Commercial - Eco", 20) },
                        { "CommercialLesiure", new ConfigOption("Commercial - Lesuire", 25) },
                        { "CommercialTourist", new ConfigOption("Commercial - Tourist", 25) },
                        { "CommercialLow", new ConfigOption("Commercial - Low", 10) },
                        { "CommercialHigh", new ConfigOption("Commercial - High ", 20) },
                        // Industrial
                        { "IndustrialGeneric", new ConfigOption("Industrial - Generic", 5) },
                        { "IndustrialForestry", new ConfigOption("Industrial - Forestry", 5) },
                        { "IndustrialFarming", new ConfigOption("Industrial - Farming", 1) },
                        { "IndustrialOil", new ConfigOption("Industrial - Oil", 5) },
                        { "IndustrialOre", new ConfigOption("Industrial - Ore", 5) },
                        // Office
                        { "OfficeGeneric", new ConfigOption("Office - Generic", 10) },
                        { "OfficeHightech", new ConfigOption("Office - Hightech", 30) }
                    }
                )
            },
            {
                "Multipliers",
                new ConfigOptionsGroup(
                    "Multipliers",
                    new Dictionary<string, ConfigOption>() {
                        { "Level", new ConfigOption("Level", 0.1) },
                        { "Population", new ConfigOption("Population", 1.1) },
                        { "Area", new ConfigOption("Area", 1.0) }
                    }
                )
            }
        };
    }
}