﻿using ColossalFramework.UI;
using System;
using UnityEngine;

namespace BulldozeNIMBY.UI {
    public class UILabelBulldozeCost : UILabel {
        public override void Start() {
            base.Start();

            name = "BulldozeCost";
            text = "Bulldoze cost: $0.00";

            isInteractive = false;
            textScale = 1.25f;
            useOutline = true;
            SetPosition();
        }

        public override void OnEnable() {
            base.Start();

            SetPosition();
        }

        private void SetPosition() {
            anchor = UIAnchorStyle.CenterHorizontal;
            relativePosition = new Vector3(0f, -32f);
        }
        
        public void SetCost(int cost) {
            text = "Bulldoze cost: $" + Convert.ToDouble(cost / 100).ToString("0.00");
        }
    }
}
